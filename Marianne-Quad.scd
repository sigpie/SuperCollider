
(~files = [
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/Astronomie_01.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/Deligny_01.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/Galilée_02.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/Galilée_03.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/Galilée_04.wav",
	// "/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/Galilée_05.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/gherasimLucaMorte_eq_02.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/gherasimLucaMorte_eq_03.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/gherasimLucaMorte_eq_04.wav",
"/home/pierre/Musique/Musique maison/Prana Cotta/paragraphes/gherasimLucaMorte_eq_05.wav"
];
x.free; x = Routine({ inf.do({ b.free; b = Buffer.read(s, ~files.choose.postln); (30.0.rand + 8).wait}) }); x.play
)


x.free
x.play

(b.free; b = Buffer.read(s,
	// "/home/pierre/Musique/Musique maison/Marianne/Deligny - Il tourne sur lui même.wav"
	// "/home/pierre/Musique/Recs/uma conferencia/english_portugues stereo/A Lecture (stereo english-portugues version).wav"
	"/home/pierre/Musique/Recs/Indonésie forêt/ThunderChickenChurch_0002/ZOOM0002_TrLR.WAV"
	// ~files.choose
);)



(Ndef(\play, {arg bufnum = b.bufnum, rate=0.8, start=0, dur=1, amp=1, loop=1, vmix=(-1), room=10, time=2;
	var sig, verb;
	sig = PlayBuf.ar(2, bufnum,rate, 0, startPos:start,loop:loop);
	// verb = GVerb.ar(sig, room, time);
	// sig = XFade2.ar(sig, verb, vmix);
	sig*amp;
}))

Ndef(\play).play.stop

Ndef(\play).set(\amp, 1, \vmix,(-1))
Ndef(\play).set(\vmix, (-0.75)) // Un  peu de reverb
Ndef(\play).map(\amp, Ndef(\amp, {LFPar.kr(6*LFNoise0.kr()).range(0, 0.5) + 1}))
Ndef(\play).set(\time, 1.5, \room,10, \vmix, -1)

Ndef(\play).map(\vmix,Ndef(\verb, {LFNoise2.kr(3).range(-1, 1)}))


(Ndef(\filt, {
	var sig = Ndef(\play);
	var amp = Amplitude.ar(sig);
	var pit = Pitch.kr(sig,execFreq:4100).lag(0.01).clip(0,600);
	sig = Array.fill(3, { arg i;
		var snd = PitchShift.ar( Ndef(\play), 0.2, 1 + ( (i+1)/60 ) * LFNoise1.kr(0.05).range(0.9, 1.2) );
		snd = DelayC.ar( snd, 0.1, LFNoise1.kr(0.025).range(0, 0.09));
		// snd = SinOsc.ar( LFNoise1.kr(0.01).range(200, 600) ) ;
	});
	sig = SelectXFocus.ar( LFNoise1.kr(0.4).range(0, 2), sig, LFNoise1.kr(0.4).range(0.5,2))
	* amp!2
	// ; sig = RHPF.ar(sig, pit, amp + 1)
	; Limiter.ar(sig,1);
}).play)


Ndef(\play).set(\amp, 0.25)
Ndef(\play).set(\rate, 1)
Ndef(\play).map(\rate, Ndef(\slow, {Lag.ar( 1 - Trig1.ar( DetectSilence.ar(Ndef(\play), amp: 0.2, time: 0.3), LFNoise0.kr(0.01).range(1, 8) ) , LFNoise1.kr(0.25).range(0.1, 1))}))
Ndef(\play).map(\rate,Ndef(\sl, {Line.kr(1,0, 30)}))
Ndef(\play).map(\amp, Ndef(\amp, {LFNoise1.kr(7) }))

Ndef(\quad, { Decay2.kr(Impulse.kr(LFNoise0.kr(0.1!2).range(1,4)), 0.01, 0.2) * SinOsc.ar(440) * 0.5}).play

Ndef(\wh, { WhiteNoise.ar( LFSaw.kr(4, [0, 1])) +  SinOsc.ar(80) * 0.1 }).play

Ndef(\ss, { SinOsc.ar(Line.kr(60, 120,30)) * 0.4!2}).play


Ndef(\a, {Pulse.ar(2) * 0.125!2}).play

Ndef(\b, {SinOsc.ar(LFPulse.kr(1.01).range(77,87)!2) * Ndef(\a) + Saw.ar(0.6) * SinOsc.ar(400*LFPar.kr(LFNoise0.kr())) }).play


// TUTO QUADRI

(
Ndef(\tuto, {
	var sig; sig =
	Pulse.ar(0.3) + Saw.ar(0.5 * LFPulse.kr(0.25,add:))
	// ;sig = Slew.ar(sig)
	// sig = GVerb.ar(sig, 140 , 1);
	;Limiter.ar(sig, 0.8!2)
	;
}).play)
Ndef(\tuto).fadeTime=0
Ndef.clear(20)

(Ndef(\dronebahn, { arg amp=0.1;
	var sig;
	sig = NumChannels.ar(
			Mix.fill(6, {arg i;
				SinOsc.ar(
					LFNoise1.kr(0.03 * i).range(60,180),
					LFSaw.kr( LFNoise1.kr(i+1/20+0.05) ).range(0, pi)
				)
			}) * 0.5
	) ;
	sig = sig + HPF.ar(sig, VarSaw.kr( 0.15, 0, LFNoise1.kr(0.1).range(0.01, 0.95)).range(50, 2000) ) ; // Vent qui bat dans les oreilles
	sig * amp * 0.6;
}).play.stop)



Ndef(\dronebahn).stop

(Ndef(\ant, {arg fq = 0.03, amp=0.1; // GROSSES ANTENNES QUI TOURNENT
	Mix.fill( 6, {arg i;
		var freq = i+1*1.25 * [100, 120]/2;
		var rate = i+1*0.1 +1 * fq;
		SinOsc.ar( freq * LFPar.kr(rate * 1.02).range(1, 1.5) * LFTri.kr(rate * 1.1).range(1, 1.5) * LFTri.kr(rate * 1.08,1.5).range(1, 1.5) + freq,0.1)
	})
	* SinOsc.ar([2000, 1000],0, 0.2,0.5)

	+ Mix.fill(2, {arg i;
		var rate = i+1*0.2 +1 * fq;
		Pulse.ar(300 + (i*10)) * LFPar.kr(rate, pi/(i+1)) * 0.75
	})
	* amp * 0.1 * LFSaw.kr(LFPulse.kr(2).range(0, 0.1) )
}).play)

Ndef(\ant).stop
Ndef(\laser).clear(10); Ndef(\car).stop
.fadeTime=10

Ndef(\ant).set(\fq, 3, \amp, 0.3).clear(4)

Ndef(\dd, { Rotate2.ar( Ndef(\ant), Ndef(\dronebahn), LFPar.kr(0.125).range(-1, 1) )  }).play


(Ndef(\car, {arg fq = 0.1, amp=0.3; // Un Choc de voiture
	Mix.fill(12, { arg i;
		BBandPass.ar(
			WhiteNoise.ar() + BrownNoise.ar(),
			LFNoise1.kr(LFNoise1.kr(0.01).range(0, 0.01 )).range(60,(i+1*150).postln),
			LFNoise1.kr(LFNoise1.kr(0.01).range(0, 0.01)).range(0, 0.5) * Env.perc(0.01, 1/fq).kr(0,Impulse.kr(fq)), // Remplace Impulse par Dust2 pour décaler un peu tout
			0.3!2
		)
	})
	// * LFPulse.kr(fq,0, 0.75, 1, LFSaw.kr(fq/2))
	+ Formant.ar(fq, LFPar.kr(fq*4).range(140, 800), 50 * LFPar.kr(fq/3) + 80, LFTri.kr(fq/4, [0, 0.5]).range(0, 0.2)) // Hi-Kick
	* amp
}).play)


Ndef(\car).play.set(\fq,0.1, \amp, 0.3)
// Augmente la fréqence pour faire le battement de voiture (+ que 8 sinon ça fait techno transe)


(Ndef(\liq, { arg amp = 0.1; // Frequence bete monte vers le bordel
	var sig;
	sig = SinOsc.ar( LFNoise1.kr( LFNoise1.kr(0.125).range(200, 380) ).range(200 , 400 + Line.kr(0, 3000, 180) ) )
	* (SinOsc.ar( [289, 300] * LFSaw.kr([1, 1.05]) * LFTri.kr([0.8, 0.86]) * LFSaw.kr([1.28, 1.12]) * LFPar.kr([0.9, 0.91]) + 2000, 0, 0.5) + 0.5)
	+ NumChannels.ar( SinOsc.ar( LFNoise2.kr(0.05!5).range(400, 600)) * LFPar.kr(7) ) // 5 SINUS plus clairs
	* (NumChannels.ar( SinOsc.ar( LFNoise2.kr(0.05!5).range(400, 600)) ) * 0.75 + 0.5) // Modulation plus lourde
	* 0.3!2 * amp;
	// BBandPass.ar(sig, 1200, XLine.kr(10,0.01,/*Dur fade*/ 2)); // Fade Out
	// BBandPass.ar(sig, 200, XLine.kr(0.01,10,/*Dur Fade*/ 4)); // Fade In
}).play)



(Ndef(\laser, {arg pulse=0.125, freq = 0.5, nz=1;
	var sig = Mix.fill(3, {arg i;
		var fq = 6 * (i+1*10) * (1 + Env.perc(0.01, LFNoise0.kr(0.25)).kr(0, Impulse.kr(pulse))) ;
		Saw.ar(fq, AmpCompA.kr(fq, 100, 0.1, 0.5)) + SinOsc.ar(fq, 0, 0.5)
	});
	sig = sig + ClipNoise.ar(LFNoise2.kr(freq) * LFPar.kr(freq) * Env.perc(0.01, 0.5/freq, nz).kr(0, Impulse.kr(pulse)));
	sig = BBandStop.ar(sig,
		LFPar.kr(freq, [0, 0.5]).range(100, 10000),
		LFSaw.kr(freq*0.5).range(0, LFNoise1.kr(0.1).range(0, 10))
	);
	// sig = sig  * Line.kr(0, 1, 180);
	sig * 0.25!2 * Line.kr(1, 0, 40);
}).play)

Ndef(\laser).set(\freq, 0.5)
Ndef(\laser).map(\freq, Ndef(\flaz))
Ndef(\laser).clear

Ndef(\quest, { VarSaw.ar( LFNoise2.kr(0.1!4).range(300, 800), 0, LFNoise1.kr(0.2!4).range(0.1, 0.9) ) * LFNoise2.kr(0.1!4) * 0.1  }).play
Ndef(\quest).clear(20)


Ndef(\flaz, { LFPulse.kr(1).range(0.25, 1) })

Ndef(\orgs).ar(4)

(Ndef(\orgs, { arg amp=0.1, qt = 0.1, rel=0.125;
	var env,trig, fq, dur;
	trig = Dust2.kr(qt) + Impulse.kr(0.25);
	dur = TExpRand.kr(0.01, 2, Gate.kr(trig, LFPulse.kr(LFNoise0.kr(0.25).range(0, rel))) );
	env = Env([0,1,0],[LFNoise0.kr(0.1).range(0.001,0.2), LFNoise0.kr(0.5).range(0.5,rel) ] * dur );
	fq = Env({ TRand.kr(50,3000,Gate.kr(trig, LFPulse.kr(LFNoise0.kr(0.25).range(0, 0.5))))}!4, {TRand.kr( 0, 0.3, Gate.kr(trig, LFPulse.kr(LFNoise0.kr(0.25).range(0, 0.5))))}!3 * dur );
	Pan2.ar( Array.fill(4,
		SinOsc.ar( fq.kr(0, trig) * Mix.kr( LFNoise1.kr(0.1!3).range(0.95, 2) ) ), LFNoise1.kr(LFNoise1.kr(0.1).range(0, 2)).range(-1, 1) )
	)  * env.kr(0,trig) * amp;
}).play)

Ndef(\orgs).set(\qt, 0.4)
Ndef(\orgs).set(\rel, 0.00125)
Ndef(\orgs).stop.clear

Ndef(\orgs).map(\qt, Ndef(\qt, { LFNoise0.kr(0.3).range(0, 8)}))
Ndef(\orgs).map(\rel, Ndef(\rel, { LFNoise0.kr(0.5).range(0, 1)}))


(SynthDef(\plot, {arg amp=0.1, dur=1; var sig, env;
	env = Env.perc(0.01, dur).kr(2);
	sig = Mix.fill(2, {arg i; Pulse.ar(i+1*1.5, Rand(0.0, 1))});
	sig = Limiter.ar(sig * amp, 0.8);
	Out.ar(0, sig!2);
}).add)

Pbindef(\yo, \instrument, \plot, \dur,4, \amp, Pwhite(0, 0.2)).play.stop

(Ndef(\plot, {
	var sig = Pulse.ar(1.5/2) + Pulse.ar(0.5) + LFPulse.ar(0.75, 0, LFSaw.kr(4).range(0.1, 0.8), SinOsc.ar(122) * 0.1)
	+ Pulse.ar(0.12, LFNoise1.kr(0.125).range(0.1, 0.8), SinOsc.ar(107) * 0.1) + Pulse.ar(0.6, 0.1, 0.1);
	sig = PitchShift.ar(sig, 0.5, LFNoise1.kr(0.01).range(0.5, 5) * Line.kr(1, 5, 30));
	Limiter.ar(sig!2,0.5);
}).play)

Ndef(\plot).fadeTime=1

(Ndef(\clear, {
	SinOsc.ar([89, 93] * Line.kr(1, 0, 0.1) * LFNoise1.kr(0.01!4).range(0.9, 1))
	+ ClipNoise.ar(LFSaw.kr(0.4) * LFPulse.kr(0.3 * LFNoise1.kr(1!4)) * LFNoise2.kr(0.2!4))
	* Line.kr(1,0, 0.30)
}).play)
Ndef(\clear).clear

(Ndef(\bruit, {arg dur=0.1, freq=2;
	// var trig = Impulse.kr(4);
	freq = freq* LFNoise1.kr();
	RLPF.ar(
		ClipNoise.ar(LFNoise2.kr(8!2)),
		Env.perc(0.01, dur,4800).kr(0, Impulse.kr(freq)),
		Env([0.1, 3], [dur]).kr(0, Impulse.kr(freq));
	) * 0.75!2
	* Env.perc(0.01, dur).kr(0, Impulse.kr(freq))!2
	+ ( Dust2.ar(LFNoise2.kr(3).range(1, 130), SinOsc.ar(189*LFSaw.kr(7).range(0.7, 5)) ) )
}).play)

Ndef(\bruit).set(\dur, 1, )