//  Jack to Bluetooth with pulseaudio :
// pactl load-module module-loopback source=jack_in



(Ndef(\s, { // BELL
	Decay2.kr(Impulse.kr(1 + LFPulse.kr(0.75) * TIRand.kr(1, 3, Impulse.kr(0.5)) / 10), 0.05, 5 )
	* Mix.fill(8, { arg i;
		SinOsc.ar(
			300
			+ (i * 0.75 + 1 * 35.5 * LFNoise1.kr(0.1).range(1, 1.5 ) ),
			0,
			((1.5 - (i * 0.5) ).abs)
			)
		* LFNoise1.kr(0.5).range(0, 1)
	})
	* 0.2!2
}).play)


(Ndef(\s, { //
	var sig, trig;
	trig = Impulse.kr( 1 + LFPulse.kr(0.75) * TIRand.kr(1, 3, Impulse.kr(0.5)) / 10);
	sig = Mix.fill(8, { arg i;
		Saw.ar(
			300
			+ (i * 0.75 + 1 * 35.5 * LFNoise1.kr(0.1).range(1, 1.5 ) )
			* Decay2.kr(trig, 0.01, 0.125).range(0.5, 1),
			((2 - (i * 0.15) ).abs)
			)
		* LFNoise1.kr(0.5).range(0, 1)
	});
	sig = RLPF.ar(sig, [200, 500], 0.2) + RHPF.ar(sig, [2000, 6000], [0.05, 0.02], [0.4, 0.25]);
	sig * Decay2.kr( trig, 0.05, 5 ) * 0.2!2
}).play)

Ndef(\ss, { SinOsc.ar(1800 * Line.kr(1, 0.6, 120)) * 0.05!2}).play