o = Server.local.options;
o.numOutputBusChannels = 4; // The next time it boots, this will take effect

(
~numChannels = 2;
/// *** ! Init and BOOT ! **** ///

// Before starting server
Server.local.options.numOutputBusChannels = ~numChannels;

~nChan = Server.local.options.numOutputBusChannels;
s.boot;
m = ServerMeter.new(s, 2, ~nChan);
)


(SynthDef(\topi, {arg out = 0, amp=0.1, dur=1, rate=1, freq=1200, rel=0.25;
	var env,sig;
	env = Env.perc(0.01, rel, amp,-8).kr(2,1);
	sig = SinOsc.ar(freq);
	sig = sig * env;
	Out.ar(out, sig);
}).add)

Pbindef(\a, \instrument, \topi, \out, Prand([0, 1, 2, 3],inf), \dur, Pseq([1, 1, 1, 2],inf) / Prand([6, 4],inf) / 12, \freq, [400, 250, 387, 678, 1300], \rel, Pwhite(0.1, 0.01) , \amp, 0.01 ).play

Pbindef(\all, \rel, Pwalk((1..20) / 10, 1, Prand([1, -1]) ))
Pbindef(\all).quant = 4

Pbindef(\b, \instrument, \topi, \out, 0, \dur, Pseq([2, 3],inf) * 5, \freq, Pseq([1400, 1250, 1387],inf), \rel, 0.125).stop

Pbindef(\c, \instrument, \topi, \out, 1, \dur, Pseq([1, 5],inf) * 4, \freq, Pseq([1000, 1450, 1387],inf), \rel, 0.25).stop

Pbindef(\d, \instrument, \topi, \out, 0, \dur, Pseq([1, 2],inf) * 4, \freq, Pseq([1100, 1210, 1387],inf), \rel, 0.125).stop

Pbindef(\all, \instrument, \topi, \out, 1, \freq, Pseq([1380, 1078, 1040], inf) / 4, \rel, Pwhite(0.01, 0.2), \dur, Prand([0.25, 0.125],inf) * 4, \amp, 0.01 ).play

NdefMixer(s)

(Ndef(\fiz, {arg amp=0.1;
	RHPF.ar(
		WhiteNoise.ar(0.1),
		LFSaw.ar( 1 / TIRand.kr(1, 8, Impulse.kr(0.25, 0.5)!4) ).range(200, 7000),
		LFSaw.ar( 1 / TIRand.kr(1, 8, Impulse.kr(0.125)!4) ).range(0.01, 0.25)
		* LFPar.ar( 1 / TIRand.kr(1, 4, Impulse.kr(0.125, 0.33)!4) / TIRand.kr(1, 8, Impulse.kr(0.125)!4) ).range(0.1, 0.5)
		,
		LFPulse.ar( 0.5 / TIRand.kr(1, 8, Impulse.kr(0.125, 0.75)!4) , 0, LFSaw.kr(2).range(0.125, 0.0025) ).range(0, 1)
	)
	* amp
}).quant_(4).play(0, ~nChan))



(Ndef(\biz, {
	var sig, env;
	env = Env.perc( LFNoise0.kr(0.1!4).range(0.1, 0.001), LFNoise1.kr(0.1!4).range(5, 7), 1, LFNoise1.kr(0.5!4).range(-8, -3)).kr(0, Impulse.kr( 0.5 * LFPulse.kr(0.25).range(0.5, 1) * LFPulse.kr(0.125, 0, LFNoise0.kr(1).range(0.1, 0.9) ).range(0.5, 1) / 4 ) );
	SinOsc.ar(
		LFSaw.kr(env * 4 / 4).range(4000, 4100)
		+ LFPar.kr(env * 3 / 10).range(100, 200)
		* 5
		// + LFSaw.kr( 1 - env + 0.1 * 6 / 10).range(0, 600)
		* LFPar.kr(LFNoise1.kr(1!4).range(0.9, 6)).range(1,  1.3)
	)
	* env
	* 0.1
}).play(0, ~nChan))
Ndef(\biz).clear(2)
Ndef(\biz).fadeTime=10

Pdef.clear; Ndef(\wool, { Formant.ar( LFSaw.kr(0.01, 1 / [1, 2, 3, 4] ).range(20, 0.01)  * LFNoise1.kr(1!4).range, LFSaw.kr([1, 2, 3, 4]/40).range(600, 5000), 599 * LFPar.kr( LFSaw.kr(0.5, 1 / [1, 2, 3, 4]).range(0, 0.75)).range(0.2, 1) * LFNoise1.kr(1!4).range, 0.1) * LFNoise2.kr(4!4).range * LFNoise2.kr(0.1!4).range }).play(0, ~nChan);

Ndef(\wool).play(0, ~nChan).fadeTime=10

Ndef(\wool).clear(30)


(Ndef(\nz, {  Pan4.ar(
	BrownNoise.ar( LFNoise1.kr(4).range * 0.1 * LFSaw.kr(0.25) ),
	LFSaw.kr(0.1).range(-1, 1),
	LFSaw.kr(0.11).range(-1, 1),
	0.1
)}).play(0, ~nChan))


Ndef(\sin, {arg har=1; Pan4.ar( SinOsc.ar( LFNoise1.kr(0.1!2).range(1900, 2200) / [1, 2, 1.5, 3, 2.5, 3.25] * TChoose.kr(Impulse.kr(0.125/2), Scale.choose.ratios).lag(6), 0, 0.1), LFNoise1.kr(0.1).range(-1, 1), LFNoise1.kr(0.2).range(-1, 1) ) * 0.1 }).play

Ndef.clear(40)

Ndef(\sin).fadeTime=10

Ndef(\sin).copy(\son)

Ndef(\son).set(\har, 2).play

Ndef(\nz).fadeTime=4

Ndef(\noise, { ClipNoise.ar( LFNoise2.kr(3!4) * LFNoise1.kr(1!4) * LFNoise1.kr(3!4) ) + SinOsc.ar( LFSaw.kr(6, [0, 0.25, 0.5, 0.75]).range(300, Line.kr(300, 1600, 130)) * LFPar.kr(2,  [0.75, 0.5, 0.25, 0] ).range(1,1.25) ) * LFNoise1.kr(3!4).range(0.1, 0)}).play(0, ~nChan)

Ndef(\noise).clear(2)


Ndef(\bd, {arg amp=0.1; var trig = Impulse.kr(0.5 * LFPulse.kr(0.125 / 2, 0, 0.33).range(1, 2)); Mix.fill(7, { SinOsc.ar( TExpRand.kr(60, LFNoise0.kr(0.1).range(300, 400), trig).lag(0.1) ) }) * Env.perc(0.01, 0.3).kr(0, trig) * amp }).play(0, ~nChan)

Ndef(\bd).set(\amp, 0.05)