{SinOsc.ar()}.play;
a=scope(s);
a.cycle=(1/440*s.sampleRate);
a.xZoom



// INSTRUMENTS

Server.local.scope

// INSTRUMENT
// Smoothly morph across different wavetables

(SynthDef(\vosc, { arg out=0, index=0, numBufs=2, freq=102.4, dcOffset=(-0.05), posMod=1;
	var sig, bufpos;
	bufpos = LFTri.kr(0.01).range(0, numBufs-1) * posMod + index;
	sig = VOsc.ar(bufpos, freq, 0, 0.1!2) + (dcOffset);
	Out.ar(out, sig);
}).add)

(SynthDef(\scope, { arg bus, bufnum, cycle;
		var z;
		z = In.ar(bus);
		ScopeOut2.ar(z, bufnum, 2048*2, cycle);
		Out.ar(0,z);
	}).add)

x.free; x = Synth(\vosc, [\buf, ~buf[0].bufnum, \numBufs, ~buffers.size]);

// START SCOPE AND SYNTHS
(s.waitForBoot({
	f = Buffer.alloc(s,1024,1);
    d = Bus.audio(s,1);

    w=Window("Scope", Rect(1200, 100, 600, 400)).front;
	e = ScopeView(w.view, w.view.bounds.insetAll(10, 10, 10, 10)); // this is SCScope
	e.bufnum = f.bufnum;
	e.background= Color.white;
	e.waveColors = Color.black!2;
	e.fill=0;
    e.server_(s);

	// making noise onto the buffer
	x.free; x = Synth(\vosc, [\out, d.index, \index, ~buf[0].bufnum, \numBufs, ~buffers.size]);

	y.free; y = Synth.tail(0, \scope, [\bus,d.index, \bufnum, f.bufnum, \cycle, 1 ] );

    // IMPORTANT
    e.start;
	~scope = s.scope(1); ~scope.yZoom = 6; ~scope.xZoom = 0.29719469103046;

	// MEMO // e -> Scope view   // x -> Synth for sound // y -> Synth for scope //

    w.onClose={x.free; y.free; d.free; f.free; ~scope.quit};
    CmdPeriod.doOnce({w.close;});

}))


~scope = s.scope; ~scope.yZoom = 6; ~scope.xZoom = 0.29719469103046;
~scope.xZoom = 0.29719469103046
~scope.xZoom = 0.59353345147848
~scope.yZoom = 6
~scope.cycle
~scope.background = Color.white
~scope.xZoom
~scope.cycle = 1024 * 100.reciprocal *4

e.xZoom = 0.59353345147848
e.xZoom = 0.29719469103046
e.xZoom = ~scope.xZoom
e.yZoom=6
y.set(\cycle, ~scope.cycle)
y.set(\cycle, 2)
y

x.set(\freq, 100)
x.free; x = Synth(\vosc, [\out, d.index, \index, ~buf[0].bufnum, \numBufs, ~buffers.size, \freq, 100, \posMod,0] );
y.free; y = Synth.tail(0, \scope, [\bus,d.index, \bufnum, f.bufnum, \cycle, 1 ] );
x.set(\posMod, 1, \index, 7, \freq, 1000, \dcOffset,-0.05)
~buf[~buf.size-1].bufnum


x
y



( // STEP 1 // READ FROM FILES
~files = [
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa1.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa2.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa3.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa4.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa5.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa6.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa7.txt", true, true,Char.comma) ),
(FileReader.readInterpret("/home/pierre/Bureau/soundwaves/promenade autorisée/faux ordre/aa8.txt", true, true,Char.comma) );
])


( // STEP 3 //// FILL BUFFERS
~buf = Buffer.allocConsecutive(~buffers.size, s, 2048);
~buf.do({arg buf, i; buf.loadCollection(~buffers[i].asWavetable)}));


( // STEP 2 //// COLLECT AND CORRECT DATAS
~buffers = ~files.collect({arg item, i;
	( a = item;
b = Array.fill( a[a.size-1][1] + 1, {List.new}); // Create an empty list the same size than the number of lines of image file
a.do{|sub| b[sub[1]].add(sub[0])}; // Ajoute la position x de chaque pixel noir sur chaque ligne


// Remove all values breaking line continuity
b.do {
	arg list, i;
	var mod = false;

	if ( i > 0, {
		var prev = b[i-1][0];
		var fools = [];

		list.do{ arg val, j;
			var delta;

			if( prev != nil ,
				{ delta = ( val - prev ).abs}, // delta entre valeur actuelle et precedente
				{delta = 0; } // pas de valeur precedente, delta = 0
			);

			if ( delta > 100, {
				mod = true; // There is a problem
				fools = fools.add(j); // Blacklist this element
			})
		};

		// Remove blacklisted from array
		if ( mod, {
			fools.reverse.do{ arg val; list.removeAt(val); }
		} );
	})
};

// Remplace les positions des pixels noir de chaque ligne par la moyenne de toutes ces positions
b = b.collect{|vals| vals.mean};

// b.plot


// Populate a Signal with each point
c = Signal.fill( b.size , {arg i; b[i];});

// Normalize to a -1 +1
c = c.normalize * 2 - 1;


// Remove random values to reach a power of two
(
if ( c.size.isPowerOfTwo, {
	("Size is a power of two :" + c.size).postln;
	// Do nothing
}, {
	("Size was" + c.size + " ").post;
	(c.size - (c.size.previousPowerOf(2))).do({
		c.removeAt(c.size.rand);
	});
	("and is now " + c.size).postln;
} );
);

// c.plot;

// Convert the Signal into a Wavetable

		// ~buffers = ~buffers.add(
		// c.asSignal(c.size).asWavetable

		// );

c
)
}))