// NUNORG !

(b = Buffer.read(s, "/home/pierre/Musique/Musique maison/Pierre Pierre Pierre/recherches/nunorg.aiff" );)

(Ndef(\play, {arg rate = 1, trig = 1, amp = 1;
	var sig = PlayBuf.ar(2, b.bufnum, rate, trig);
	sig  * amp;
}).play)

Ndef(\play).map(\rate, Ndef(\rate, { Line.kr(0, 1,  rrand(1, 13))}));
Ndef(\play).map(\rate, Ndef(\rate, { Line.kr(0.5, 0.1.rand, rrand(0.1,5))}))
Ndef(\play).set(\amp, 0.01)

Ndef(\uu).map(\mov, Ndef(\mov, { Line.kr(0, 1, rrand(0.01, 3))}))
Ndef(\uu).map(\mov, Ndef(\mov, { Line.kr(1,0, rrand(0.01, 23))}))
Ndef(\uu).set(\amp, 0.0)


Ndef(\oo).map(\mov, Ndef(\moov, { Line.kr(1,0, rrand(0.01, 13))}))
Ndef(\oo).map(\mov, Ndef(\moov, { Line.kr(0,1, rrand(10.01, 23))}))
Ndef(\oo).play
Ndef(\oo).stop




Ndef(\play).set(\rate, 0)
Ndef(\play).set(\amp, 1)

Ndef(\play).clear

Ndef(\rate, { LFPulse.ar(LFNoise2.kr(0.2).range(0.05, 0.1), 0, LFNoise2.kr(0.1).range(0.01, 0.5))})

NdefMixer(s)
Ndef.clear

/////////////////////////////////////////////////////////////
/* # # # # # # # # # # # # # # # # # # # # # # # # # # # # */
/////////////////////////////////////////////////////////////


(Ndef(\nunorg, {arg amp=0.1, rate= 0.5;
	var sig, rs, rev;

	rs = LFPar.ar( LFNoise1.kr(0.1) * 1) * 10;
	rev = LFPar.ar(LFNoise1.kr(0.1) * 0.1 ).range(3, 3);

	sig =
	Mix.ar(LatoocarfianL.ar(
		SampleRate.ir / [2, 4, 8],
		LFSaw.kr( LFNoise1.kr(0.1).range(0.001, rate),1,1,1.5) + LFPar.kr(LFPar.kr(0.3).range(0.5, 2) , 0, LFNoise1.kr(0.1).exprange(0.0001, 0.5)),
		LFPar.kr( LFNoise1.kr(0.1).range(0.001, rate) ,0,0.5,2) + LFPar.kr(LFPar.kr(0.123).range(0.5, 1.5), 0, LFNoise1.kr(0.1).exprange(0.0001, 0.5)),
		LFSaw.kr( LFNoise1.kr(0.1).range(0.001, rate) ,0.5,0.5) + LFPar.kr(LFPar.kr(0.2).range(1.75, 0.5), 0, LFNoise1.kr(0.1).exprange(0.0001, 0.5)),
		LFSaw.kr( LFNoise1.kr(0.1).range(0.001, rate) ,0.5,0.5,1) * LFNoise2.kr(0.4).range(0.1, 1),
	)
	* LFNoise1.kr([8, 2, 1])
	* LFNoise2.kr([8, 2, 1].reverse)
	);

	sig = GVerb.ar(sig,
		rs,
		rev,
		LFPar.kr(LFNoise1.kr(4) + 1 ).range(0.01, 1) ,
		LFSaw.kr(3 * LFPulse.kr(1).range(1, LFNoise0.kr(1).range(0.35, 2))).range(1, 2) * LFNoise2.kr(0.25).range(0.1, 0.75),
		15 * LFNoise1.kr(16),
		1,
		1 * LFNoise1.kr(0.1),
		earlyreflevel:LFNoise1.kr(0.3).range(0.01, 0.7),
		taillevel: LFNoise1.kr(0.3).range(0.01, 0.5)
	);

	sig = sig * LFNoise2.kr( LFNoise1.kr(0.5!4).range(0.001, 2) ).range(0.25,1) ;
	sig = HPF.ar(sig, 30);
	sig = sig * amp;
}))


Ndef(\nunorg).play
Ndef(\nunorg).clear

Ndef(\nunorg).set(\rate, 0.15, \amp, 0.03)

Ndef(\nunorg).map(\rate, Ndef(\nrate, { (LFNoise2.kr(0.1).range(0.001, 1) * ( Schmidt.ar( Ndef(\nunorg), 0.01, 0.02) - LFPulse.kr(LFNoise0.kr(0.1).range(0, 1)).range(0, 1) ).abs ) }))
Ndef(\nunorg).map(\amp, Ndef(\nunamp, { LFPar.kr(0.01).range(0, 0.1) * LFSaw.kr(0.0789).range(0, 1) * LFNoise1.kr(0.2).range(0.25, 1) }))

Ndef.clear

//////////////////////////////////////////////////////////////////////////////
/////#################################////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


Ndef(\verb, { GVerb.ar( Ndef(\play), 30, 2, 0.2) * 0.3 }).play

Ndef(\uu).set(\mov, 0.5)

(Ndef(\uu, { arg amp=0.1, mov=1;
	var sig, reso, tmp;

	sig = NumChannels.ar(
		Formant.ar(
			LFNoise2.kr(0.02!3).exprange(3, 60),
			LFNoise2.kr(0.01!3).exprange(300, 2000),
			LFNoise2.kr(LFNoise1.kr(0.1!3).range(0, 0.02) ).range(100, 1900)
	), 1)
	+ SinOsc.ar( TIRand.kr( 30, 120, Impulse.kr([0.2, 0.3, 0.25] * mov / 2 ) ).midicps.lag(5) * 0.5, 0, 0.1 * LFNoise2.kr(0.05) + 0.1).sum;
	tmp = LFNoise2.kr(0.05 * LFNoise2.kr(5)).exprange(0.01, 0.2) ;

	reso = Formlet.ar( sig,
		TIRand.kr( 50, 160, Impulse.kr([0.2, 0.3, 0.25] * mov / 10) ).midicps.lag(6),
		tmp,
		tmp * LFNoise2.kr( 2 * mov ).range(1.125, LFNoise1.kr(0.1).range(1, 3) )
	).sum!2;

	sig = reso + (sig * reso);
	sig = NumChannels.ar(sig, 1)!2;
	sig = Balance2.ar(sig[0], sig[1], LFNoise2.kr(1).range(-0.75, 0.75)) * 0.1;
	sig = sig * amp;

}).play)

Ndef(\uu).set(\amp, 0.2, \mov, 0.5)

Ndef(\uu).fadeTime=2

Ndef(\uu).clear


Ndef(\oo).set(\mov, 0.2)
(Ndef(\oo, {arg mov=1;
	var trig, fonda, sig, nuno;

	trig = Impulse.ar(1 * LFPulse.kr(0.5, 0, 0.75) * LFPulse.kr(0.75, 0, 0.75) * LFPulse.kr(0.5, 0, 0.5, 0.5) * mov )  ;

	fonda = Stepper.ar( trig , 50, 60, 70, TIRand.kr(-3, 5, trig) * LFPulse.kr(0.5) );

	sig = Mix.fill(4, {arg i;

		Decay2.ar(  TDelay.ar(trig, TRand.kr(0, i+0.1, trig) ) ,0.01, 3 / (mov+0.05) )

		* LFSaw.kr(
			Sweep.kr(trig + Dust2.kr(0.2), LFNoise2.kr(0.2).range(0.001, 3) )
			* LFPar.kr( LFNoise2.kr(0.4) ).exprange(0.0125, 2)
		)

		* SinOsc.ar( ( fonda + Stepper.ar( TDelay.ar( Impulse.ar(i+1*0.165), i + 1 * 0.1) , i, 0, 12, TIRand.kr(-2, 3, Impulse.kr(0.25) ) )).midicps )
	} ) ;
	sig = sig !2;
	sig = HPF.ar(sig, 40);
	sig * 0.25;
}).play;)

Ndef(\oo).fadeTime=30

Ndef(\oo).map(\mov, Ndef(\oomov, { LFNoise2.kr( LFNoise1.kr(0.1).exprange(0, 2) ).exprange(0.0001, 1) }))
Ndef(\oo).set(\mov, 0.1)

Ndef(\oo).stop

Ndef.clear

(Ndef(\kick, { arg amp=0.3;
	var sig, ctrl, trig, fq ;
	ctrl = Ndef(\nunorg);
	ctrl = Amplitude.ar(ctrl, 0.01, 0.1) ;

	trig = Schmidt.ar(ctrl, 0.001, 0.1);

	trig = Sweep.kr(trig) > 2 * trig ;

	Poll.kr(Impulse.kr(10),trig,  \trig);

	// fq = EnvGen.kr( Env([2800, 180, 90], [0.01, 0.3]) , trig);
	fq = 87;

	// fq = fq * 87;
	// fq = EnvGen.kr(fq,Impulse.kr(0.66));
	// fq = fq * 87 * LFNoise2.kr(17  ).range(1, 1.2);
	// fq = 87;

	sig = SinOsc.ar(fq ) + Saw.ar(fq) + WhiteNoise.ar();

	sig = NumChannels.ar(RLPF.ar( sig, fq * 1.3, 0.2), 1)!2;

	sig = Env.perc().kr(0, trig ) * sig;

	sig * amp;
}).play.stop)


(Ndef(\tambour2, {arg rate = 1, amp=0.1;

	var sig = Saw.ar( TIRand.kr(3, 8, Ndef(\pianotrig) ) * LFPulse.kr( 2 ).range(0.5, 1) * rate ) * LFNoise1.kr(37) * 0.2;

	var env = Decay2.kr( Dust2.kr(0.2) + Impulse.kr(0.5 * LFPulse.kr(0.75 * ( rate)).range(0.5, 1) * rate ), 0.01, 8 * LFNoise1.kr(0.05).exprange(0.3, 1) * ( 1 / rate ) );

	GVerb.ar(
		sig
		, 1, 0.54, 0.9 * LFNoise0.kr(4).range(0.7, 1)
	) * amp * env;

}).play)

Ndef(\tambour2).set(\rate, 0.25, \amp, 0.05)
Ndef(\tambour2).set(\amp, 0.1, \rate, 0.01)
Ndef(\tambour2).stop

Ndef(\pianotrig).set

Ndef(\pianotrig, {arg freq=0.25;	Impulse.kr(freq * LFPulse.kr(freq / TIRand.kr(1, 4, Dust.kr(0.1) ) ).range(0.5, 1) );})

(Ndef(\piano, {arg freq=0.25, rel=5, amp=0.1;
	var trig;

	// trig = Impulse.kr(freq * LFPulse.kr(freq / TIRand.kr(1, 4, Dust.kr(0.1) ) ).range(0.5, 1) );
	trig = Ndef(\pianotrig);

	Decay2.kr(trig, 0.05, rel) *
	Splay.ar(
		Mix.fill(7, { arg i;
		SinOsc.ar((TChoose.kr(trig, [50, 52, 57, 55, 42, 45, 38].midicps ).lag(LFNoise1.kr(0.4).range(0.01, 1)) * ( i * TChoose.kr(trig, [0.25, 0.5]) + 1)).poll(1, \note_piano) ) * 0.1
	})
	) * amp
}).play)

Ndef(\piano).set(\freq, 0.125, \rel, 8, \amp, 0.3)
Ndef(\piano).stop


(Ndef(\toup, {
	Mix.fill(5, {arg i;
		var t = TDelay.kr( TDuty.kr( Dseq([0.5, 0.75, 1.5, 1], inf )), i );
		SinOsc.ar( TRand.kr(300, 500, Gate.kr(t , LFPulse.kr(LFPulse.kr(0.5).bipolar) ) + Impulse.kr(0.25) ).poll(1, \note_toup) ) * LFNoise2.kr( LFNoise1.kr(0.1).range(10,30)).range(1, 0.99)
		* Decay2.kr( t , 0.01, 0.3)
	}) * 0.025!2
}).play)











// ########################## // // ########################## // // ########################## //

// ########################## // // ########################## // // ########################## //

// ########################## // // ########################## // // ########################## //



(Ndef(\cut, {
	Ringz.ar(
		Resonz.ar(
			LFPulse.ar(3) + LFSaw.kr(2),
			LFPar.kr(5).range(300, 90),
			0.2 )!2,
		LFPulse.kr( 8 / TIRand.kr(2, 6, Impulse.kr(4) ) ).range(300, 378),
		0.05 * LFPar.kr( 4 * LFPulse.kr(1).range(1, 2) ).range(0.001, 2) ,
		0.1 * LFPar.kr(1, 0, LFSaw.kr(3 * LFPulse.kr(2).range(1, 0.5)) )
	) * 0.5
	+ Ringz.ar( Impulse.ar( 12 * LFPulse.kr(4).range(0.5, 1) ) , LFPulse.kr(8 * LFPulse.kr(2).range(1, 0.5) ).range( LFPulse.kr(7).range(378, 520), LFPulse.kr(6).range(478, 700) ), 0.2, 0.05)
	* 0.5
}).play)


( Ndef(\paste, { arg amp=0.2;
	SinOsc.ar(4098 * LFNoise1.kr(8 * LFNoise1.kr(0.1!3) ).range(1,1.4) + LFSaw.kr(7, 0, 10, 1) ) * Env.perc(0.1 / TIRand.kr(1, 50, Impulse.kr(In.kr(~pulse)) ), LFSaw.kr(12 / TIRand.kr(1, 8, Impulse.kr(0.5!3)) ).exprange(Line.kr(0.001, 0.1, 120), 0.75) * LFPar.kr(3 * LFPar.kr(0.01).range(1, 2) * 2 ) ).kr(0, Impulse.kr( In.kr(~pulse) * 2) ) * amp!2
}).play; )

~pulse = Bus.control(s)

(Ndef(\crass, { arg osc=0.1, rate = 1;
	var sig;
	var pulse = 4 * LFPulse.kr(3).range(1, 1.5) * rate;
	var chg = Impulse.kr( 1 / TIRand.kr(6, 18, Impulse.kr(1/22)) );
	sig = LFPulse.ar( pulse );
	Out.kr(~pulse, pulse);
	sig = GVerb.ar(sig, 2, 0.1 * LFNoise2.kr(1).range(0.1, 2) , 0.2, LFSaw.kr( TIRand.kr(2, 5, chg) + 0.5 ).range(0.2, 0.5), LFSaw.kr(10).range(6, 22), 1, 0.7, LFSaw.kr(5).range(0.2, 0.9) ) * 0.1;
	sig = sig + SinOsc.ar( LFSaw.kr(rate + TIRand.kr(3, 4, TDelay.kr(chg, 4) ) * LFSaw.kr(1) ).range(79, 87) + 150 * LFSaw.kr(1/rate).exprange(1, 1.125), 0, LFPulse.kr(2 + TIRand.kr(2, 4, chg) * LFPar.kr(rate).range(1, 1.5) * ( 10 / rate) ) * 0.05 * LFSaw.kr(rate) );
	sig = sig + WhiteNoise.ar( Env.perc(0.2, 0.01).kr(0, Impulse.kr(3.25 * rate) ) * 0.015 * LFPulse.kr(7.125 * rate) * LFPulse.kr(9.125 * rate) );
	sig = SinOsc.ar( LFSaw.kr(89 * LFNoise2.kr(rate).range(1, rate)).range(560, 600) + TIRand.kr(0, 100, chg) + LFPulse.kr(pulse*1.5).range(0, 30), 0, LFNoise2.kr(78 * LFNoise1.kr(0.1).range(0.25, 1) ).range(0, 1) * LFNoise1.kr(0.1 * rate).range(0, 0.5) * LFNoise1.kr(0.1*rate).range(0,0.5)) * osc + sig;
	sig = NumChannels.ar( Splay.ar(sig), 2);
	sig;
}).play)

Ndef.clear

Ndef(\crass).set(\osc, 0.2)
Ndef(\crass).fadeTime=0.01
Ndef(\crass).set(\rate, 0.25)
Ndef(\crass).set(\rate, 1)
Ndef(\crass).map(\rate, Ndef(\rate, { Stepper.kr( Impulse.kr( 1 / 2 ), 0, 1, 20 , -1, 20) / 10 }))

Ndef(\crass).map(\rate, Ndef(\rate, { Stepper.kr( Impulse.kr( In.kr(~pulse) ), 0, 1, 50 , 1, 0) / 100 }))

Ndef(\crass).map(\rate, Ndef(\rate, { Stepper.kr( Impulse.kr( In.kr(~pulse) ), 0, 1, 1000 , -1, 0) / 1000 }))

20/10

LFNoise2.kr(0.1).range(1.2, 1) *

( Ndef(\pul, {
	var src = HPF.ar( Ndef(\crass), 2880 );
	var amp = Amplitude.ar(src);
	var rate = In.kr(~pulse);
	var trig = LFPulse.kr(  rate / 2 );
	var sig = Formant.ar(80 * LFSaw.kr(rate) + 60, 2500 * TRand.kr(0.1, 2, trig).lag(1/rate), 100 * TRand.kr(0.5, 4, trig).lag(1/rate) ) * ( LFPulse.ar(rate) + LFSaw.ar( LFPulse.kr(rate).range(rate * 0.5, rate*2 ) ) );
	sig = Env.perc(0.01, LFNoise1.kr(1).range(0.01, 1) ).kr(0, trig) * sig;
	sig = sig * LFNoise2.kr(8 * rate);
	// sig = Compander.ar(sig, src, 0.5, 1.2);
	sig = NumChannels.ar(sig, 2) * 0.1;
}).play; )

Ndef(\pul).stop
Ndef.clear


Ndef(\ki, { Env.perc(0.01, 0.2).kr(0, Impulse.kr( 0.5 ) ) * Pulse.ar( LFSaw.kr(70).range(89, 40) + LFNoise1.kr(0.1).range(0, 20) ) * 0.25!2 }).play

Ndef(\ki).copy(\kik)

Ndef(\kik).play.clear

(
Ndef(\lead, {
	Saw.ar( Env.perc(0.01, 0.1, 0.2).kr(0, Impulse.kr(4)) + 1
		* Duty.ar( Dseq( 1 / [4, 2, 8, 4, 8, 8] ,inf ), 0, Dseq([44, 62, 74, 66, 69],inf ).midicps ).lag(0.2)
		* [1, 0.98 + Env.perc(0.01, 0.3, 0.05,0.6).kr(0, Impulse.kr(1)) ] )

	* Decay2.kr( TDuty.kr( Dseq( 1 / [4, 4, 8] ,inf ) ), 0.01, LFSaw.kr(7 * LFPulse.kr(2).range(1, 0.5)).range(0.1, 0.5) )  * 0.06!2
	* Line.kr(0, 1, 60)
	// * 0
} ).play;

Ndef(\acon, {var sig;
	sig = Formant.ar( LFPulse.kr(LFPulse.kr(0.25).range(0.5, 1) ).range(4, 8) , LFSaw.kr(12).range(80, 260), 1020, 0.1);
	sig = Ringz.ar(sig, LFSaw.kr(16).range(80, 120), 0.6, 0.1) + RHPF.ar(sig, LFSaw.kr(8).range(300, 340), 0.005, 0.3);
	sig = Decay2.kr(TDuty.kr( Dseq( 1 / [4, 8, 8] ,inf ) ), 0.01, 0.1) * SinOsc.ar(LFPar.kr(64).range(76, 87) ) * 0.7 + sig;
	sig !2;
}).play
)

Ndef.clear


//
// DRUMMM FRIEND
//

~thresh = 0.03



Ndef(\a).fadeTime=20; Ndef(\b).fadeTime=20


(Ndef(\a, {arg thresh=0.05;
	var sig;
	var in = In.ar(2);
	var amp = Amplitude.ar( in );
	var trig = Trig.ar( in > LFNoise2.kr(0.05).range(thresh , thresh * 4) );
	var time = Timer.ar( trig ).max(0.001);
	time = time.lag();
	trig = TIRand.kr(0, 1, trig);

	time.poll(2, \time);

	sig =
	[ Saw.ar( time )
	, Saw.ar( 1 / time )]
	* [0.1, 0.1];
	sig = sig * Env.asr().kr(0, trig);
	sig = Compander.ar(sig, sig, 0.2, 1, 0.1, 0.01, 0.01);
	Limiter.ar(sig,0.9);
}).play;


Ndef(\b, {arg thresh=0.05;
	var sig, delta;
	var in = In.ar(2);
	var amp = Amplitude.ar( in );
	var trig = Trig.ar( in > LFNoise2.kr(0.05).range(thresh , thresh * 4) );
	var pitch = Pitch.kr(in);
	var time = Timer.ar( trig ).max(0.001);
	var freq = LFNoise2.kr(0.05!2).range(60, 200);
	trig = TIRand.kr(0, 1, trig);
	trig.poll(2, \trig);
	sig = SinOsc.ar( freq ) + Formant.ar( [time, 1/time] * freq, pitch.lag(30), pitch.lag(10) ) * 0.05 * Env.asr().kr(0, trig);
	Compander.ar(sig, sig, 0.2, 1, 0.1, 0.01, 0.01);
}).play; )


(Ndef(\blips, { arg rel=0.25, thresh=0.05;
	var env, sig, mod, freq, hasFreq, vol, pitch, modArr, modLenArr  ;
	var gate=1;
	var in = In.ar(2);
	var amp = Amplitude.ar( in );
	var trig = Trig.ar( amp > LFNoise2.kr(0.05).range(thresh , thresh * 4) );
	var time = Timer.ar( trig ).max(0.001);
	trig = ( ( Sweep.ar(trig) < LFNoise2.kr(0.01).range(2, 15) ) + trig / 2 ).ceil ;
	trig = trig * LFPulse.ar(time / 10 );
	# freq, hasFreq = Pitch.kr(in);
	vol = Latch.ar(amp, trig);
	pitch = Latch.ar( freq , hasFreq );
	pitch = pitch * Phasor.kr(trig, 0.1,1, 0, 1);
	mod = LFNoise2.kr( LFNoise0.kr(1).range(0.1, 0.4) ).range(0.5, 2) * pitch;
	mod = mod.lag(amp) * Phasor.kr(trig, 0.1,1, 0, 1);
	mod = LFPar.kr( mod ).range(0.25, 2.75) * LFSaw.ar( mod ).range(0.5, 2);
	env = Env.adsr(0.01, 0.2, vol, rel).kr(0, trig);

	pitch = pitch.lag( LFNoise0.kr(0.1).exprange(0.01, 30)  );
	// mod = mod.lag( LFNoise0.kr(0.1).exprange(0.01, 30) );

	// pitch = pitch.lag( Sweep.kr(trig, 0.1) );
	mod = mod.lag( Sweep.kr(trig, 0.1) );

	sig = SinOsc.ar( pitch * ( mod + 1 ) ) + SinOsc.ar( pitch ) + SinOsc.ar( (mod / pitch).abs );

	// sig = GrainIn.ar(2, Impulse.kr(time), 0.25, sig, LFNoise2.kr(2),);
	sig = sig * env!2;
	sig = Compander.ar(sig, sig, 0.2, 1, 0.1, 0.01, 0.01);
	Limiter.ar(sig,0.9);
}).play
)



