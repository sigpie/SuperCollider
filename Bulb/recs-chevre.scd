
(b.free; b = Buffer.cueSoundFile(s,
	// "/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Doom1-002.wav"
	// "/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Rock 1-002.wav"
	"/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Rap 1-002.wav"
	// "/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Relax 2-002.wav"
, 0, 2, 32768*2 );)

(c.free; c = Buffer.cueSoundFile(s,
	// "/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Doom1-003.wav"
	// "/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Rock 1-003.wav"
	"/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Rap 2-003.wav"
	// "/home/pierre/Musique/Musique maison/Bulb/Recs de la chèvre/Garage du jeudi/Relax 2-003.wav"
, 0, 2, 32768*2 );)


Ndef.clear

( Ndef(\play, {arg amp=1, rate=1, freq=8900, bw=30, pan=0;
	var sig = BBandPass.ar(
		VDiskIn.ar(2, b, rate, 1) * amp,
		freq,
		bw
	);
	sig = Balance2.ar( sig[0], sig[1], pan);
}).play;

Ndef(\play2, {arg amp=1, rate=1, freq=8900, bw=30, pan=0;
	var sig = BBandPass.ar(
		VDiskIn.ar(2, c, rate, 1) * Saw.kr(1).range(0, 1) * amp,
		freq,
		bw
	);
	sig = Balance2.ar( sig[0], sig[1], pan);
}).play)


Ndef(\rate, { LFPulse.kr(LFPulse.kr(1).range(0.25, 0.5)).range(0.75, 1) })

Ndef(\amp, {LFPulse.kr(LFPulse.kr(1.5).range(1, 2)).range(0.25, 0.8)})


Ndef(\play).set(\amp, 0.5)

Ndef(\play).map(\rate, Ndef(\rate), \amp, Ndef(\amp))

Ndef(\play).stop; Ndef(\play2).stop
Ndef(\play).play; Ndef(\play2).play


Ndef(\new, { SinOsc.ar( SinOsc.ar(60).range(350, 400) ) * 0.1!2}).play

Ndef(\new, { Mix.fill(100, { SinOsc.ar( LFNoise1.kr(0.1).range(100, 1600) ) * 0.01 })}).play