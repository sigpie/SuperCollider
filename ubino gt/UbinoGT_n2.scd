/// LIVE CODING


(SynthDef(\live, { arg amp=0.4, dur=1, hi=300;
	var env, sig, dura, mod;
	dura = Env([1,1], [dur]).kr(2);
	env = Env([0, 1, 0.5, 0], [0.01, dur*0.125, dur*Rand(0.125, 1)], Rand(-6, 6)!3).kr(0);
	mod = Env([10, 1, 0.25 ], Rand(0,dur/2)!2, Rand(-3, 3)!2).kr(0);

	sig = Mix.fill(12, {arg i; SinOsc.ar(Line.kr(100, 200, dur * TChoose.kr(1, [6, 5, 4, 3])) * (i+1), pi/2, 0.6)}) ;
	sig = SinOscFB.ar([146, 156] * TChoose.kr(1, [0.75, 2, 1]) * mod, Line.kr(0, 2, Rand(dur, 0) ), 2) * TChoose.kr(1, [0.25, 0.5, 0.75]) + sig;
	sig = sig * SinOsc.ar(2000 * (mod * [ env, 1/env] ), pi/2, 1 * TChoose.kr(1, [0, 0, 0, 1]), 1) ;
	sig = SinOsc.ar( LFNoise2.kr(14!2).range(2000, 3500)) * TChoose.kr(1, [1, 0, 0]) + sig;
	sig = RLPF.ar(Saw.ar( TChoose.kr(1, [300, 289, 178, 274])), Env([hi, 60], [dur/4], Rand(-6, 6)).kr(0), 0.001, 2) * LFNoise0.kr(2!2).range(0, 2) + sig;
	sig = sig * LFNoise0.kr(2!2).range(0, 1);
	Out.ar(0, sig * amp * env );
}).add)

Pbindef(\li, \instrument, \live, \dur, 1 / Pwalk( (2..8), Pwrand([0,1,-1], [4, 1, 1].normalizeSum, inf ) ), \amp, 0.1).play
Pbindef(\li, \dur, Pseq([1/8, 1/8, 1/4],inf) )
Pbindef(\li, \dur, Pseq([1/8],inf) )
Pbindef(\li, \dur, Prand( [Pseq([1/8], 4), Pseq([1/8, 1/8, 1/4], 4)], inf), \amp, 0.1 * Pseq([1, 0.5, 1, 0.25, 0.75],inf) ).stop

Pbindef(\li, \dur, 1 / Pseq([8, 8, 6],inf) / Pwrand([1, 4], [0.8, 0.2],inf), \amp, 0.1 * Pseq([1, 0.5, 1, 0.25, 0.75],inf) )

Pbindef(\li, \dur, Pseq([1/8],inf), \instrument, \live).play

Pbindef(\li, \hi, Pseq((60*2..380*4)*5, inf)*0+300)


Ndef(\mv, { LFNoise1.kr(1).range(0, 6)})


(Ndef(\sin, { arg amp=0.1;
	Mix.fill(8, {arg i;
		var fq = (200 + (i*14) * (i+0.33 * 3)) + LFNoise1.kr(Ndef(\mv)).range(0, 800);
		SinOsc.ar( fq, 0, 0.1 * AmpCompA.kr(fq));
	})
	* amp!2
}).play)

Ndef(\sin).set(\amp, 0.1)

(Ndef(\sinsin, {
	Ndef(\sin) * Ndef(\sin2)
	* XLine.kr(0.5, 1, 3)
}).play)

Ndef(\sin2).stop(60)


o = Server.local.options
o.memSize
o.free
o = ServerOptions.new
o.memSize = 8192*4

s.reboot
s.killall

s.options.memSize = 8192*4

(Ndef(\voice, {

	Splay.ar(
		Mix.fill(6, { arg i;
		PitchShift.ar(
			DelayC.ar(
				In.ar(2, 2), 1, 2.5* i
			)
		 * 0.2 + Saw.ar(18) ,
		0.5,
		A2K.kr(In.ar(2,2)).range(i+1*2*(-1), i+1*2*2)
	)
	})
		// *0
		+ Pulse.ar(LFPulse.kr(0.25).range(100, 2300), 0.2, 0.5)
		* VarSaw.kr(7, 0, LFNoise1.kr(0.2))
		// *0

		// + BrownNoise.ar(LFPulse.kr(0.77 * LFPulse.kr(1).range(1, 2), [0, 1], 0.33) * LFSaw.kr(0.3) )
		// * 0

		// + Formant.ar( LFPulse.ar( 7 ).range(6, 8) + LFPulse.kr(0.25).range(0, 12), [899, 267, 698, 900] * LFSaw.kr(8).range(0.8, 1), 50 * LFPulse.kr(4).range(0.25, 1),0.5)

)
	+
	(
		(Decay2.ar(Impulse.ar(0.1), 0.01, 0.1,( SinOsc.ar( Pitch.kr( A2K.kr(In.ar(2, 2))) + 300, 0, Amplitude.ar(In.ar(2,2)) ) * 0.5)))
		+ LFPulse.ar( LFSaw.kr(4) * LFPar.kr(1.26) * LFNoise1.kr(0.1).range(100, 300) * LFTri.kr(0.156).range(1, 3), 0, LFSaw.kr(1.11) * LFPulse.kr(0.87) * 0.5, LFSaw.kr(0.7)*LFPar.kr(0.2))

		+ LFSaw.ar( Pitch.kr(In.ar(2)) , 0, 0.1, Amplitude.kr( In.ar(2) ) + 0.1, 0.5)
		+ SinOsc.ar([3000, 3190] * LFSaw.kr(100) + 400 * LFNoise1.kr(10).range(0.75, 1.25), 0, 0.1 * LFNoise2.kr(30).range(0.001, 1) )

	)

}).play)

Ndef(\voice).fadeTime=0.5


///////////////////////

(Ndef(\un, {
	Mix.fill(6, { arg i;
		SinOsc.ar(490 * (i+1*0.23), 0, LFSaw.kr(i+1*[2.5, 3.5]) * LFPulse.kr(i+1*[3, 2])) * 0.25
	})
	* LFPar.kr([4, 6], 0, LFPulse.kr([8, 3]).range(0.5, 1.5))
	* ( LFPulse.kr(0.33,0, LFNoise1.kr(0.1).range(0.1, 0.9)).range(1, SinOsc.ar([3290, 3230], 0, LFPulse.kr(3 * LFPulse.kr(1.5, [0, 1]).range(1, 10)).range(0.5, 1), 1) ) )
	+ ClipNoise.ar(LFSaw.kr([3, 2] * 2 * LFPulse.kr(0.75*2, [0, 0.75]).range(1.5, 1) * LFPulse.kr(1.5*2, 0, 0.9)) * PinkNoise.ar(LFSaw.kr([3.125, 3])) * LFPulse.kr(0.25))

	+ SinOsc.ar([100, 95] + LFSaw.kr(17.8 * LFSaw.kr([0.17, 0.2, 0.4, 0.1], [0, 0.2, 0.34, 0.7]), 0, 1200) * 1, 0, LFPulse.kr(18, [0, 1]) * LFSaw.kr( 0.12 ))
}).play)

Ndef(\un).fadeTime=0


(Ndef(\dx, {
	Saw.ar(80 * LFNoise0.kr(0.125).range(0.8, 1))
	* Saw.kr([6.33, 6])
	+ ( LFSaw.kr([3, 3.33]*100) * LFPulse.kr(6)
		* Pulse.kr([0.3, 0.25]*4, LFNoise1.kr(0.1), LFPulse.kr(6), 0.5)
	)
	* 0.5 * LFNoise2.kr(18 * LFNoise1.kr(3)*2)
	// * ClipNoise.ar(LFSaw.kr(0.25))
	// + (LFPulse.kr([3, 3.3]*3) * SinOsc.ar(1604) * 0.1)
}).play)

Ndef(\tr, {Impulse.kr(0.5)})

(Ndef(\qt, {
	Decay2.kr(
		Ndef(\tr), 0.01, LFNoise1.kr(0.2) * 2
	) * SinOsc.ar(777 * Phasor.kr(Ndef(\tr), LFSaw.kr(1),0, LFPulse.kr(0.5).range(2, 1)) + LFPulse.kr(7).range(0, 478))
}).play)

Ndef(\qt).fadeTime=0.5


Ndef(\tr, {Env.perc(0.01, 0.1).kr(0, Impulse.kr( TChoose.kr(Impulse.kr(1), [2, 4, 8, 8, 16, 16]*4) ))})

(Ndef(\se, {
	Ndef(\tr) *
	RLPF.ar(
		Pulse.ar(LFCub.kr(1).range(120, 1580)) + ClipNoise.ar()
		, LFSaw.kr(0.017).range(2540, 260), LFNoise1.kr(16).range(0.1, 0.6)
	)
	* 0.3!2
}).play)

Ndef(\ba, { RLPF.ar( Saw.ar([130, 120] * LFPulse.kr(1).range(1, 1.5)), 1600 * LFNoise1.kr(0.2).range(0.1, 2), LFNoise2.kr(2).range(0.01, 0.3), 0.2 * LFPulse.kr(4).range(0.5, 2))}).play

Ndef(\ii, {Splay.ar( SinOsc.ar([1200, 1289] * [1, 0.6, 1.6, 1.2, 0.76, 2.7] * LFSaw.kr(1).range(0.9, 1.2) * LFNoise1.kr(0.2!5).range(0.6, 1.2)) )* 0.075}).play

Ndef(\po, {Mix.fill(10, {SinOsc.ar(Rand(100, 700) * LFSaw.kr(1).range(1, 0.25), 0, 0.1)})}).play