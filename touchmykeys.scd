(Server.local.options.numOutputBusChannels=8;
~nChan = Server.local.options.numOutputBusChannels)

~nChan = Server.local.options.numOutputBusChannels

s.boot;
s.quit

(Ndef(\test, {arg rate=2, freq = 880;
	var sig;
	var env = Env.perc(0.01, 0.1).ar(0, Impulse.kr(8));
	// sig = SinOsc.ar( LFPar.kr(LFNoise1.kr(0.1!9).unipolar).range(240, 1500)) * LFPulse.kr(LFNoise1.kr(1!9).unipolar).unipolar.lag(0.05);
	sig = SinOsc.ar(200 + (env*4400)) + ClipNoise.ar(0.25);
	sig = SinOsc.ar(LFPar.kr(8+8.rand).range(1, 1.2) * freq) * sig;
	sig = PanAz.ar(9, sig, (( Stepper.kr(Impulse.kr(rate), 0, 0, 15, 2) + 1 ) / 9 ).lag(1/rate), 1, 1);
	// sig = PanAz.ar(9, sig, TChoose.kr(Impulse.kr(rate), (0..7) * 2 ) + 1 /9 , 1, 1);
	sig = sig * env;
	sig = sig * 0.1;
}).quant_(1).play(0, ~nChan))





//////////////////////////        ///////////////////////////////////////
////////////////////////// FOR 8 OUTPUTS  ///////////////////////////////////////
///////////////////////////////        ///////////////////////////////////////

(Ndef(\group, {
	Array.fill(4, { arg i, amp = 0.05, rel=0.5, att=0.01;
		var trig, env, sig, rate, phase;
		amp = 1;
		phase = IRand(1, 4) / 3;
		rate = 4/4;
		rate = rate * TIRand.kr(1, 3, CoinGate.kr(0.3, Impulse.kr(rate / 8, Rand(0, 1.0) ) ) );
		trig = Impulse.kr(rate , phase);
		att = att * (1/rate) / 2;
		rel = rel * (1/rate) / 2;
		Env.perc(att, rel).kr(0, trig * LFPulse.kr( rate / 8, LFNoise0.kr(rate/8).unipolar, LFNoise2.kr(rate/8).range(0.5, 1)) )
		* SinOsc.ar(Rand(400, 600)
			* LFPar.kr(Rand(400, 600),0, LFNoise0.kr(rate/8) * LFNoise2.kr(rate/16).range(0.5, 1.5) / (i+1) , 1 ) )
		* LFNoise2.kr(rate/16).range(0.25, 1)
		* amp
	})
	* 0.5
});

Ndef(\x).reshaping = true;
Ndef(\group).play(0, ~nChan)
)

Ndef(\group).fadeTime=20
NdefMixer(s)



////////////////////   A     ///////////////////////////////////////
////////////////////////// PLAYER  /////////////////////////////////
///////////////// THAT  SOUND'S     ////////////////////////////////


(b.free; b = Buffer.read(s,
	// "/home/pierre/Musique/Recs perso/Tascam 4 track test repetition.WAV"
	// "/home/pierre/Musique/Recs perso/ZOOM0034_TrLR - Paysage et frottements.WAV"
	// "/home/pierre/Musique/Recs perso/ZOOM0027_TrLR - Bon Pour l'HP.WAV"
	"/home/pierre/Musique/Recs perso/ZOOM0026_TrLR Chantier Pont Bourgogne.WAV"
); // remember to free the buffer later.
c.free; c = Buffer.read(s,
	"/home/pierre/Musique/Recs perso/ZOOM0001_TrLR - Solo Piano.WAV"
	// "/home/pierre/Musique/Recs perso/ZOOM0062_Tr2 - Water Plops GlouGlou.WAV"

);)


(Ndef(\play, { arg rate = 1, start = 0, amp = 0.5;
	Limiter.ar(	PlayBuf.ar(2, b.bufnum, BufRateScale.kr(b.bufnum) * rate, 1, BufFrames.kr(b.bufnum) * start, 1) * amp, 1 );
}).play(0, 2))

Ndef(\play).set(\amp, 5, \rate, 1)
Ndef(\play).set(\amp, 5, \rate, 0.5) // Pour le CHANTIER du PONt

(Ndef(\play).set(\rate, (Scale.minor.ratios.choose / 4 + 0.25).postln );
Ndef(\play).lag(\rate, 1.0.rand))



(Ndef(\player, { arg rate = 1, amp = 0.5;
	var sig = PlayBuf.ar(2, c.bufnum,
		BufRateScale.kr(b.bufnum) * rate,
		Impulse.kr(MouseX.kr(0, 10)),
		BufFrames.kr(b.bufnum) * (MouseY.kr(0.4, 0.5) * MouseX.kr()),
		1);
	sig = sig * amp;
	sig = Limiter.ar(sig, 1 );
}).play(0, 2))

Ndef(\player).set(\rate, 0.5, \amp, 0.7)

              ______
///////////// ...../////////////////
/////////// ...../////////////////
///////// ...../////////////////
// T H E        M A T R I X

(Ndef(\matrice, { arg amp = 0.05, rel=0.5, att=0.01, qt = 1;
		var trig, env, sig, rate, phase;
		amp = 1;
		phase = Rand(0, 1.0);
		rate = 3/4 * qt;
	rate = rate * TIRand.kr(1, 3, CoinGate.kr(0.3, Impulse.kr(rate / 8, Rand(0, 1.0) ) ) );
	trig = Impulse.kr(rate , phase);
	att = att * (1/rate);
	rel = rel * (1/rate);
	env =  Env.perc(att, rel).kr(0,trig * LFPulse.kr( rate / 4, LFNoise0.kr(rate/16).unipolar, LFNoise2.kr(rate/16).range(0.25, 1)) );
	// * SinOsc.ar(Rand(400, 100) * LFPar.kr(Rand(400, 600),0, LFNoise0.kr(rate/8) * LFNoise2.kr(rate/16).range(0.5, 1.5),1) )
	sig = Formant.ar(
		Rand(4, 300),
		Rand(180, 1200) * LFPar.kr(Rand(400, 600)/10,0, LFNoise0.kr(rate/8) * LFNoise2.kr(rate/16).range(0, 1) / 4 / rel,1),
		Rand(180, 4000) * LFPar.kr(Rand(400, 600),0, LFNoise0.kr(rate/8).unipolar * LFNoise2.kr(rate/16).range(0, 1), 1 )
	)
	* LFNoise2.kr(rate/16).range(0.125, 1);
	sig = sig * env;

	// For Stereo Version ONLY v v v
	sig = Pan2.ar(sig,
		TChoose.kr(Dust2.kr(0.05), [-1, 1, 0]).lag(5)
	);
	sig = sig * amp;
});

~s.do({arg item, i; item.clear;});
~s = 6.collect({arg i; var name = "matrixed" ++ i.asString; Ndef(\matrice).copy(name) });
~s = ~s.add(Ndef(\matrice));
~s.do({arg item, i; item.set(\qt, 0.25, \rel, 1); }); // Start Rare

// Launch and map v v v
// ~s.do({arg item, i; item.play(1, i%~nChan); }); // Multichannel managed !
NdefMixer(s);
)
~s
Ndef.clear

NdefMixer(s)

~s.do({arg item, i; item.play(i+2,1)});
~s.do({arg item, i; item.set(\qt, [0.25, 1].choose, \rel, 0.5) });
~s.do({arg item, i; item.set(\qt, 1, \rel, 0.5) });
~s.do({arg item, i; item.play;});
~s






  //////////////////////////   ACTIVE  ///////////////////////////////
 ///////////////  FINGERS      //////////////////////////////////////
//////////////////////////   O v o //////////////////////////////////

/// Use > "arduino_supercollider_boilerplate.scd" to send values into buses ///

// // // // // // // // // //
 // // // // // // // // //
//  Trig a DUMMY Synth
// // // // // // // // // //
 // // // // // // // // //

(SynthDef(\dummy, {arg amp=0.15, freq = 440; var env, sig;
	env = Env.perc(0.01, 5).kr(2, 1);
	// MODIFIER ICI
	// freq = freq * LFPulse.kr(6).range(1, 1.25) * LFPar.kr(67).range(0.8, 1);
	// amp = amp * LFNoise1.kr(15);
	sig = SinOsc.ar(freq) * env * amp;
	Out.ar(0, sig!2);
}).add;)


(~dummy.stop; ~dummy = Routine({
	~play = 0!8;
	loop{ b.get({ arg val; val.do{arg item, i;
				if( (item == 1 ) && ( ~getValue.(b, 3) == 1 ) && (i !=3 ),{ if ((~play[i] == 0), { Synth(\dummy, [\freq,  i + 1 * 120]);~play[i] = 1; })},{ ~play[i] = 0; });}}); 0.01.wait; };
}).play)

~dummy.stop

// Function to catch one value a specific button / pot
~getValue = {arg bus, chan; var r; bus.getnSynchronous(chan+1)[chan] }


// // // // // // // // // //
 // // // // // // // // //
// Trig an enveloppe in a bus
// // // // // // // // // //
 // // // // // // // // //

( Ndef(\form, {
	var sig, fq;
	fq = (Env.adsr().kr(0, In.kr(b.index+1)) * (In.kr(p.index+1) * 4 + 100) + 4).floor;
	sig = Formant.ar(
		(Env.adsr().kr(0, In.kr(b.index)) * In.kr(p.index) / 5).floor,
		fq,
		(Env.adsr().kr(0, In.kr(b.index+2)) + 0.01 * (In.kr(p.index+2) * 10) + 1).floor
		// In.kr(b.index+2)  * In.kr(p.index+2),
	);
	sig = Limiter.ar(sig, 0.02 ); // AMPLITUDE CONTROL
	// sig = sig * AmpCompA.kr((fq));
	sig = Pan2.ar(sig, ( (Mix.kr(In.kr(p.index,3)) / 3069 * 2 - 1) * (LFPulse.kr(LFNoise0.kr(0.1).range(0.01, 0.5) ).lag(0.2).range(-1, 1)) ));
}).play(0, 2);
)


b.get({arg item; item.postln;})
Ndef(\form).clear
// INIT !!

Ndef(\form).clear

d[0].index
d[0].set(10)
d[1].set(200)
d[2].set(220)

Ndef.clear
~form.stop
NdefMixer(s)

(
)

~form.stop


~vals

~envs = nil

~envs[0]

~envs =
~envs = Synth(\trig, [\channel, d[0], \param, 8] );

~port.readaaaaaaaaaa

//////////// NDEF Listening to Serial ////////

Ndef(\test, {
	SinOsc.ar(

	)
})






// TO DO  ////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//////////////////////////////////// A GUI ////////////////////////////////////




w = Window.new("GUI Introduction", Rect(200,200,320,320)).front;
// notice that FlowLayout refers to w.view, which is the container view
// automatically created with the window and occupying its entire space
w.view.decorator = FlowLayout(w.view.bounds);
14.do{ Slider(w, 150@20) };



w = Window.new(bounds:Rect(200,200,200,50)).front;
b = Button.new(w,Rect(10,10,80,30)).states_([["Off"],["On"]]);
t = StaticText(w,Rect(100,10,90,30)).string_("Button released");
b.mouseDownAction = { t.string = "Button pressed" };
b.mouseUpAction = { t.string = "Button released" };

