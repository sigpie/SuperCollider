(SynthDef(\bass, {arg amp=0.1, freq=88, dur=1, att=0.01, rel= 0.3, mod = 0.2;
	var env, sig;
	env = Env.perc(att, rel, amp).kr(2, 1);
	sig = Pulse.ar(freq * (LFSaw.kr(freq/1.5 * Rand(0.8, 1!2)).range(0, 1) * mod+1).range(0.8, 1) );
	sig = RLPF.ar(sig, Line.kr( Rand(70, 2300), ExpRand(30, 1700), dur ), LFSaw.kr(1/dur, Rand(0.0, 1)).range(0.1, 0.6) );
	sig = sig * env;
	Out.ar(0, sig);
}).add;)

~ratios = [Scale.minor.ratios[0], Scale.minor.ratios[2], Scale.minor.ratios[3], Scale.minor.ratios[4]];


(
Pbindef(\b, \instrument, \bass, \rel, Prand([0.3, 0.4, 0.6, 1], inf) , \dur, Pwrand([Pseq([0.25], 4), Prand([3/4, 0.25, 0.25, 0.5, 0.25, 0.25], 4) ], [0.9, 0.1],inf) * 0.75 , \freq, [66 , 66 * Pstutter(2, Prand(~ratios, inf) )], \amp, 0.1 ).play;

// Pbindef(\b, \freq, [68, 73], \amp, 0.05);

Pbindef(\c, \instrument, \bass, \rel, Pwhite(0.7, 0.4) * Pkey(\dur, inf), \dur, Prand([0.5, 0.25],inf) * 0.75, \freq, 66 * 4 * Prand(Scale.minor.ratios!3, inf), \mod, 1, \amp, 0.035).play)