s.quit
(o = Server.local.options; // Get the local server's options
o.numOutputBusChannels = 2; // The next time it boots, this will take effect
o.numInputBusChannels = 2; // The next time it boots, this will take effect
s.boot;
)



(Ndef(\tape, { arg freq=4;
	SinOsc.ar(LFNoise1.kr(0.1).range(100, [300, 3600, 1000, 1600])) * 0.05 * LFNoise2.kr(4)
}).play)


(SynthDef(\tape, {arg out=0, amp=0.1;
	var env, sig, mod;

	sig = {
		var mod = Env.dadsr(Rand(0, 0.1), Rand(0.01, 0.3), Rand(0.1, 0.3), Rand(40, 5000), Rand(0.1, 1), Rand(40, 5000)).ar(0, 1);
		var env = Env.perc(Rand(0.01, 0.25), ExpRand(0.1, 1)).kr(2, 1);
		SinOsc.ar(mod, 0, AmpCompA.ar(mod)) * amp * env;
	}!4;
	Out.ar(out, sig);
}).add;)


Pbindef(\go, \instrument, \tape, \dur, 1).play

Ndef(\sin, { SinOsc.ar( LFPulse.kr(4).range(400, 450) * LFNoise1.kr(0.1!2).range(0.9, 1) ) * 0.1}).play


(Ndef(\follow, {arg amp=0.01, lag=6;
	var freqA = Pitch.kr(SoundIn.ar(0));
	var freqB = Pitch.kr(SoundIn.ar(1));
	var freq;

	freqA[0] = freqA[0].lag(LFNoise1.kr(0.1).range(1, 10));
	freqB[0] = freqB[0].lag(LFNoise1.kr(0.1).range(1, 10));

	freq = ( freqA[0] * freqA[1] ) + (freqB[0] * freqB[1]) * ( (freqA[1] + freqB[1]).lag(0.1) * 2 % 3 / 2);

	freq = freq.lag(lag);

	(SinOsc.ar( freq ) * (freqA[1] + freqB[1]).poll
	* AmpCompA.kr(freq))

	* amp;
}).play)

(Ndef(\in, {
	SoundIn.ar(2)
	+ SoundIn.ar(3)
	* 0.2;
}).play)


Ndef(\in).play
Ndef(\follow).stop

Ndef(\follow).set(\amp, 0.05)
Ndef(\follow).set(\lag, 0.5)