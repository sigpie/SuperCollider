TRILHA AMANDA


(SynthDef(\pi, {arg
	SinOsc.ar()
}).add)

Scale.directory
Scale.at(\chinese).ratios

a =  Scale.minor.ratios; a.removeAt(1); a.removeAt(3);
a = a++ (a*2)
a
{rand(4)+1 * 2}!8

(Ndef(\hair, {arg amp=0.1, fund=40;
	var trig, sig, env, dur, scale, ratio;
	// scale = Scale.choose;
	scale = Scale.at(\chinese);
	scale.name.postln;
	scale = scale.ratios;
	scale.removeAt(1);
	scale.removeAt(3);
	scale = scale ++ (scale[0] * 2);
	scale = scale ++ (scale[2] * 2);
	scale.size.postln;
	scale = scale.scramble;
	scale.postln;
	// dur = TCh
	// trig = Impulse.kr( TWChoose.kr(Impulse.kr(4), (2..8), [1, 2, 3, 4, 2, 1, 1].normalizeSum ) );

	// RANDOM RYTHME WALK
	// trig = Impulse.kr( TWChoose.kr(Impulse.kr(4), (1..4) * 2, [1, 2, 3, 1].normalizeSum ) );

	// SELECTED RYTHM WALK
	trig = Select.kr( Stepper.kr(Impulse.kr(2), 0, 0, 4), { rand(4) + 1 }!4 ).poll(2);

	//  MANUAL RYTHM WALK
	trig = Select.kr( Stepper.kr(Impulse.kr(2), 0, 0, 8),  [2, 3, 3, 2, 4, 1, 3, 3] ).poll(2);

	trig = Impulse.kr(trig);

	env = Env.perc(0.05, TRand.kr(0.125, 1/2, trig), amp).kr(0, trig);

	// STEADY RATIO WALK
	ratio = Select.kr( Stepper.kr( trig, 0, 0, scale.size - 1,
		// Random Steps
		// TWChoose.kr(CoinGate.kr(0.5, trig), [2, 1, -2, -1, 4, -4 ,0 ], [4, 2, 4, 2, 1, 1, 2].normalizeSum )
		// LFPulse.kr(1/3).range(-1, 2)
	), scale );


	// ratio =  TWChoose.kr( CoinGate.kr(0.8, trig), scale, [4, 1, 3, 1, 2, 1, 2].normalizeSum );

	// RANDOM OCTAVES
	sig = SinOsc.ar( fund.midicps * TChoose.kr(CoinGate.kr(0.75, trig), (1..4) ) * ratio );
	sig = Saw.ar( fund.midicps * TChoose.kr(CoinGate.kr(0.75, trig), (1..4) ) * ratio ) + sig;

	// NO OCTAVES
	// sig = SinOsc.ar( fund.midicps * ratio );
	// sig = Saw.ar( fund.midicps * 4 * ratio ) + sig;

	sig = RLPF.ar(sig, fund.midicps * TChoose.kr(CoinGate.kr(0.75, trig), (3..6) + 2 ), Env.linen(0.01, 0, TRand.kr(0.05, 0.5, trig), 1).kr(0, trig) + 0.05 );
	sig = sig * env;
}).quant_(4).play(0, 2))

Ndef(\hair).set(\fund, 48)

(SynthDef(\synth, {arg amp=0.1, att=0.01, rel=0.5, freq=440, pan=0, dur=1;
	var sig, env, side;
	side = LFSaw.kr(1 / dur * Rand(0.5, 2)).unipolar;
	env = Env.perc(att, rel).kr(2);
	sig = SinOsc.ar( freq ) * [side, 1-side];
	sig = Saw.ar( freq * IRand(1, 4) ) * [1-side, side] + sig;
	sig = RLPF.ar(sig,  freq * IRand(5, 8) / IRand(1, 2), Env.linen(att, 0, Rand(0.1, 1.5) * rel).kr(0) + 0.05 );
	sig = sig * env;
	sig = Pan2.ar(sig, pan);
	sig = sig * amp;
	Out.ar(0, sig);
}).add)

Pbindef(\lead, \instrument, \synth, \dur, 1 / Pseq([3, 3, 3, 4, 4, 4, 4],inf) , \freq, 48.midicps * Pseq( [a[0], a[0], a[2], a[4], a[2] * 2, a[1] ], inf), \rel, 0.25 ).quant_(4).play

Pbindef(\follow, \instrument, \synth, \dur, 1 / Pseq([2, 2, 1],inf) , \freq, 48.midicps * Pseq( [a[0], a[0], a[2], a[4] ], inf)  * 3, \rel, 0.25 ).quant_(4).play.stop;

Pbindef(\followlow, \instrument, \synth, \dur, 1 / Pseq([Pseq([4], 8), Pseq([0.5])],inf) / 2 , \freq, 48.midicps * (Pseq( [a[0], a[0], a[2], a[4] ], inf)) * [1, 2],\pan, Pseq([-1, 1],inf), \rel, 0.15, \amp, 0.05 ).quant_(4).play

Pbindef(\followlow).stop

NdefMixer(s)

a = Scale.at(\hirajoshi).ratios
Scale.directory

Ndef(\hair).stop
Ndef(\hair).play

(Ndef(\touc, {arg amp=0.1;
	var sig, env, die;
	die = Line.kr(1, 0, 30);
	env = Env.perc(0.01, LFSaw.kr(1/6).range(0.5, 1) * 6, 1, -12).kr(0,Impulse.kr( 1 ) );
	sig = Saw.ar( LFPar.kr(3/6 * die) + LFPar.kr(6.5/6*die) + LFSaw.kr(8.3/6*die) + 4 * 160);
	sig = Saw.ar( LFPar.kr(10.0.rand/6*die) + LFPar.kr(10.0.rand/6 *die) + LFSaw.kr(10.0.rand/6*die) + 4 * 50) + sig;
	// sig = sig * env;

	sig = RHPF.ar(sig, 350, 0.12);
	sig = LPF.ar(sig, 1800 * LFPulse.kr(1/3).range(1, 0.5));

	// sig = ClipNoise.ar(0.6) * Env.perc(0.01, 0.25, 1, -6).kr(0, Impulse.kr(1 / LFPulse.kr(0.25, 0, 0.75).range(4, 2), 0.75) ) + sig;
	sig = LPF.ar(sig, 3800);
	sig = sig * amp!2;
	sig
}).quant_(4).play(0, 2).stop)

(Ndef(\toc, {
	var sig, env;
	env = Env.perc(0.01, 1, 1, -6).kr(0, Impulse.kr( 1 / LFPulse.kr(0.25, 0, 0.75).range(4, 2) ) );
	sig = SinOsc.ar( LFPar.kr(48).range(70, 220), pi/2, 0.5 );
	// sig = sig + ClipNoise.ar(0.3);
	sig = RHPF.ar(sig, 70, 0.12);
	sig = LPF.ar(sig, 1800 * LFPulse.kr(1/3).range(1, 0.3));
	sig = sig * env * 0.3!2;
	sig
}).quant_(4).play(0, 2))

Ndef(\toc).stop
Ndef(\toc).play

Ndef(\tic, { Formant.ar(4 * LFPulse.kr(1/2).range(1, 0.5)  * LFPulse.kr(1/3).range(1, 2), LFPar.kr(12).range(80, 440) * 3, LFPar.kr( LFPulse.kr(1/2, 0, 0.25).range(3, 40)).range(10, 2270) ) * 0.2!2 }).quant_(4).play.stop

Ndef.stop