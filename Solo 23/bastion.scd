AEIOU
"/home/pierre/Documents/sc/aeiou - flutes/flutes - Dz - jan 2023 - exports/dialogos/dialogos pierre.wav"
"/home/pierre/Documents/sc/aeiou - flutes/flutes - Dz - jan 2023 - exports/dialogos/dialogos rébé.wav"

Bobauds
"/home/pierre/Documents/sc/DigiBauds/Digibauds 2/01-Voix-221217_2324.wav"
"/home/pierre/Documents/sc/DigiBauds/DigiBauds - 1/interchange/DigiBauds - 1/audiofiles/01-221122_2319.wav"


o = Server.default.options;

o.numOutputBusChannels.postln;

// Set them to a new number

o.numOutputBusChannels = 4;


// set default quant
Ndef(\x).proxyspace.quant = 4.0;
// set default reshaping rules : elastic reshape according to the new number of channels
Ndef(\x).proxyspace.reshaping = \elastic


(Ndef(\plic,{
	Formant.ar(LFPulse.kr(1/8, 2 / (1..4), 0.25).range(0.1, 0.25), LFNoise1.kr(21!4).range(300, 100) , LFNoise1.kr(0.1!4).range(8, 1))
	* 0.051
}).
play(0,4))

Ndef(\plic).clear(60)


(Ndef(\poc, {
	SinOsc.ar(444 * LFNoise1.kr(0.03!4).range(0.1, 2))* PinkNoise.ar(0.5)
	* Env.perc(0.01, LFNoise1.kr(0.25!4).range(0.1, 0.5)).kr(0, Impulse.kr(3, 2 / (1..4)))
	* 0.1
}).play(0, 4))


// FILE PLAYER
(b = Buffer.read(s,
	// "/home/pierre/Documents/sc/DigiBauds/Digibauds 2/01-Voix-221217_2324.wav"
	"/home/pierre/Documents/sc/aeiou - flutes/flutes - Dz - jan 2023 - exports/dialogos/dialogos rébé.wav"
);) // remember to free the buffer later.




(Ndef(\play, {arg bufnum = b.bufnum, rate=1, strt=0, trate=0, amp=0.5;
	var trig = Impulse.kr(trate);
	var sig = PlayBuf.ar(1, b.bufnum, BufRateScale.kr(b.bufnum) * rate, trig, strt * BufFrames.kr(b.bufnum), 1);
	// sig = PanAz.ar(4, sig, LFNoise2.kr(1).range(0, 2/4));
	sig = Pan4.ar(sig, LFNoise1.kr(1).bipolar, LFNoise1.kr(1).bipolar, amp);
	sig;
}).play(0, 4))

Ndef(\play).set(\trate, 1/8, \strt, 0.2)
Ndef(\play).set(\amp, 1, \rate, 0.8)

Ndef(\play).map(\trate, Ndef(\trate, { LFPulse.kr(1/6).range(0, TIRand.kr(0.5, 15, Impulse.kr(1/5))) }))
Ndef(\play).map(\strt, Ndef(\strt, { LFNoise1.kr(0.1).range(0, 0.5)}))